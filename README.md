Script used for gesture recognition in the paper "Pierre Kateb, Alice Fornaciari, Chakaveh Ahmadizadeh, Alexander Shokurov, Fabio Cicoira, Carlo Menon. Improved Vapor Phase Polymerization of Pyrrole on Textiles for Capacitive Strain Sensing and Machine Learning-Assisted Hand Gesture Recognition"

Content: Python Jupyter notebooks for machine learning training and evaluation with data from 4 capacitive sensors placed on fingers as input and gesture labels as output. 


## License
Please see the license file. 


